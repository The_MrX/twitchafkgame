﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.CoreProgram
{
    //The point of this class is to control which UI components are shown/active/hidden and coordinate with the UIController
    public class UIManager : MonoBehaviour
    {
        public static UIManager Singleton;
        
        public static readonly Resolution _2160p = new Resolution{Name = "2160p",Width = 3840, Height = 2160};
        public static readonly Resolution _1440p = new Resolution{Name = "1440p", Width = 2560, Height = 1440};
        public static readonly Resolution _1080p = new Resolution{Name = "1080p", Width = 1920, Height = 1080};
        public static readonly Resolution _720p = new Resolution{Name = "720p", Width = 1280, Height = 720};
        public static readonly Resolution _480p = new Resolution{Name = "480p", Width = 640, Height = 480};

        private FullScreenMode _currentScreenMode = FullScreenMode.Windowed;
        private Resolution _currentResolution = _1080p;


        [SerializeField] private GameObject MainMenu;
        [SerializeField] private GameObject OptionsMenu;
        
        [SerializeField]private List<GameObject> _uiPrefabs = new List<GameObject>();
        private Dictionary<string, UIObject> _prefabsDictionary = new Dictionary<string, UIObject>();
        
        
        public bool GetPrefab<T>(out UIObject prefab )
        {
            var key = typeof(T).Name;
            //Debug.Log("Name of UIObject: " + key);
            if (_prefabsDictionary.ContainsKey(key))
            {
                prefab = _prefabsDictionary[key];
                return true;
            }
            Debug.Log("No prefab of " + key + " found!");
            prefab = null;
            return false;
        }
        
        private void Awake()
        {
            Singleton = this;
            foreach (var prefab in _uiPrefabs)
            {
                var components = prefab.GetComponents<UIObject>();
                foreach (var component in components)
                {
                    //Debug.Log("Name of component: " +component.GetType().Name);
                    _prefabsDictionary.Add(component.GetType().Name, component);
                }
            }
            SetScreenMode(_currentScreenMode);
            SetResolution(_currentResolution);
        }

        public void ShowMainMenu()
        {
            MainMenu.SetActive(true);
        }
        
        public void HideMainMenu()
        {
            MainMenu.SetActive(false);
        }
        
        public void ShowOptionsMenu()
        {
            OptionsMenu.SetActive(true);
        }
        
        public void HideOptionsMenu()
        {
            OptionsMenu.SetActive(false);
        }
    
        
        public void SetScreenMode(FullScreenMode screenMode)
        {
            _currentScreenMode = screenMode;
            SetResolution(_currentResolution);
        }

        public void SetResolution(Resolution resolution)
        {
            _currentResolution = resolution;
            Screen.SetResolution(resolution.Width, resolution.Height, _currentScreenMode);
        }
        
        public FullScreenMode GetCurrentScreenMode()
        {
            return Screen.fullScreenMode;
        }
        
        public Resolution GetCurrentResolution()
        {
            return _currentResolution;
        }

    }
    [Serializable]
    public class Resolution
    {
        public string Name;
        public int Height;
        public int Width;
    }
    
}