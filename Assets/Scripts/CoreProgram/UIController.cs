﻿using UnityEngine;

namespace Game.CoreProgram
{
    //The point of this class is to handle all GUI info requests and GUI inputs from the user for all UI interactions
    //This way the GUI components do not need to interact with the ProgramState or UIManager directly.
    // And then message the UIManager and ProgramState & GameMode as needed.
    public static class UIController
    {
        public static void HandleStartGameModeClicked()
        {
            UIManager.Singleton.HideMainMenu();
            ProgramState.Singleton.StartActiveGameMode();
        }

        public static void HandleExitApplicationClicked()
        {
            ProgramState.Singleton.CloseApplication();
        }
        
        public static RectTransform GetGameModeOptionsGUI()
        {
            return ProgramState.Singleton.GetCurrentGameModeOptionsContent();
        }

        public static void HandleStopGameClicked()
        {
            ProgramState.Singleton.AbortActiveGameMode();
        }
        
        public static void HandlePauseGameClicked()
        {
            ProgramState.Singleton.PauseActiveGameMode();
        }
        
        public static void HandleResumeGameClicked()
        {
            ProgramState.Singleton.ResumeActiveGameMode();
        }

        public static void HandleSetActiveGameMode(int index )
        {
            ProgramState.Singleton.SetActiveGameMode(index);
        }

        ///Volume of 0 to 1
        public static void SetVolume(AudioMixerEnum mixer, float volume)
        {
            SoundManager.Singleton.SetVolume(mixer, volume);
        }

        public static float GetVolume(AudioMixerEnum mixer)
        {
            return SoundManager.Singleton.GetCurrentVolume(mixer);
        }

        public static void SetResolution(Resolution resolution)
        {
            UIManager.Singleton.SetResolution(resolution);
        }

        public static void SetScreenMode(FullScreenMode fullScreenMode)
        {
            UIManager.Singleton.SetScreenMode(fullScreenMode);
        }

        public static Resolution GetCurrentResolution( )
        {
            return UIManager.Singleton.GetCurrentResolution();
        }

        public static FullScreenMode GetCurrentScreenMode()
        {
            return UIManager.Singleton.GetCurrentScreenMode();
        }

        public static string[] GetListOfAvailableGameModes()
        {
            return ProgramState.Singleton.GetListOfGameModes();
        }
    }
}