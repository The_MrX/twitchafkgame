﻿using System;
using UnityEngine;

namespace Game.CoreProgram
{
    ///This Class is simply to allow easy setting of audio clips in the editor,
    /// alongside additional properties we may want to set on a audioClip individual basis.
    [Serializable]
    public class AudioObject : MonoBehaviour
    {
        [SerializeField]public AudioClip AudioClip;
        [SerializeField][Range(0,1)]public float Volume;
    }
}