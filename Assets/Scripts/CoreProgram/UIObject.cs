﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Game.CoreProgram
{
    //This Class Exists simply to make it easier to tell apart which scripts are UI related in UIManager.
    public class UIObject : MonoBehaviour
    {
        [HideInInspector]public string ClassName;

        public UIObject()
        {
            ClassName = this.GetType().Name;
        }
    }
}