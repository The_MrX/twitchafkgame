﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.CoreProgram.GUI
{
    public class GUI_OptionsMenu : MonoBehaviour
    {
        [SerializeField] private Slider _allVolumeSlider;
        [SerializeField] private Slider _musicVolumeSlider;
        [SerializeField] private Slider _effectsVolumeSlider;
        [SerializeField] private TMP_Dropdown _resolutionDropdown;
        [SerializeField] private TMP_Dropdown _screenModeDropdown;
        [SerializeField]private Button _resumeButton;
        [SerializeField]private Button _pauseButton;

        //This Must be kept In Sync with the Resolution DropDown in the Editor.
        List<Resolution> Resolutions = new List<Resolution>{UIManager._480p, UIManager._720p, UIManager._1080p, UIManager._1440p, UIManager._2160p};
    
        //This Must be kept In Sync with the ScreenMode DropDown in the Editor.
        private FullScreenMode[] _fullScreenModes = {
            FullScreenMode.ExclusiveFullScreen, FullScreenMode.FullScreenWindow, FullScreenMode.MaximizedWindow,
            FullScreenMode.Windowed
        };
        
        private void Start()
        {
            SetSlidersToCurrentVolume();
            SetDropdownToCurrentResoution(_resolutionDropdown);
            SetDropdownToCurrentScreenMode(_screenModeDropdown);
        }

        private void OnEnable()
        {
            SetListeners();
        }

        private void OnDisable()
        {
            RemoveListeners();
        }
        

        private void SetListeners()
        {
            _pauseButton.onClick.AddListener(Pause);
            _resumeButton.onClick.AddListener(Resume);
            _resolutionDropdown.onValueChanged.AddListener(SetResolution);
            _screenModeDropdown.onValueChanged.AddListener(SetScreenMode);
            _allVolumeSlider.onValueChanged.AddListener(SetAllVolume);
            _musicVolumeSlider.onValueChanged.AddListener(SetMusicVolume);
            _effectsVolumeSlider.onValueChanged.AddListener(SetEffectsVolume);
        }
        
        private void RemoveListeners()
        {
            _pauseButton.onClick.RemoveListener(Pause);
            _resumeButton.onClick.RemoveListener(Resume);
            _resolutionDropdown.onValueChanged.RemoveListener(SetResolution);
            _screenModeDropdown.onValueChanged.RemoveListener(SetScreenMode);
            _allVolumeSlider.onValueChanged.RemoveListener(SetAllVolume);
            _musicVolumeSlider.onValueChanged.RemoveListener(SetMusicVolume);
            _effectsVolumeSlider.onValueChanged.RemoveListener(SetEffectsVolume);
        }

        private void SetSlidersToCurrentVolume()
        {
            _allVolumeSlider.value = UIController.GetVolume(AudioMixerEnum.Master);
            _musicVolumeSlider.value = UIController.GetVolume(AudioMixerEnum.Music);
            _effectsVolumeSlider.value = UIController.GetVolume(AudioMixerEnum.Effects);
        }

        private void SetDropdownToCurrentResoution(TMP_Dropdown dropdown)
        {
            var currentResolution = UIController.GetCurrentResolution();
            for (int i = 0; i < Resolutions.Count; i++)
            {
                if (currentResolution.Name == Resolutions[i].Name)
                {
                    dropdown.value = i;
                }
            }
        }
        
        private void SetDropdownToCurrentScreenMode(TMP_Dropdown dropdown)
        {
            var screenMode = UIController.GetCurrentScreenMode();
            for (int i = 0; i < _fullScreenModes.Length; i++)
            {
                if (screenMode == _fullScreenModes[i])
                {
                    dropdown.value = i;
                }
            }
        }

        public void SetAllVolume(float volume)
        {
            UIController.SetVolume(AudioMixerEnum.Master, volume);
        }
        
        public void SetEffectsVolume(float volume)
        {
            UIController.SetVolume(AudioMixerEnum.Effects, volume);
        }
        
        public void SetMusicVolume(float volume)
        {
            UIController.SetVolume(AudioMixerEnum.Music, volume);
        }
        
        public void SetScreenMode(int index)
        {
            UIController.SetScreenMode(_fullScreenModes[index]);
        }
        
        public void SetResolution(int index)
        {
            UIController.SetResolution(Resolutions[index]);
        }
        
        public void Pause()
        {
            UIController.HandlePauseGameClicked();
        }
        
        public void Resume()
        {
            UIController.HandleResumeGameClicked();
        }
    }
}