﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.CoreProgram.GUI
{
    public class GUI_MainMenu : MonoBehaviour
    {
        [SerializeField]private TMP_Dropdown GameModeDropDown;
        [SerializeField]private ScrollRect _scrollRect;

        private void Start()
        {
            SetDropDownOptions();
            AddScrollViewContent();
        }

        private void OnEnable()
        {
            GameModeDropDown.onValueChanged.AddListener(OnGameModeSelected);
        }

        public void SetDropDownOptions()
        {
            var gameModes = UIController.GetListOfAvailableGameModes();
            List<TMP_Dropdown.OptionData> listOptionData = new List<TMP_Dropdown.OptionData>();
            foreach (var gamodeModeName in gameModes)
            {
                var optionData = new TMP_Dropdown.OptionData(gamodeModeName);
                listOptionData.Add(optionData);
            }
            GameModeDropDown.options = listOptionData;
        }

        public void AddScrollViewContent()
        {
            //todo Do we need to destroy the previous one?
            //Destroy(_scrollRect.content);
            _scrollRect.content = null;
            _scrollRect.content = UIController.GetGameModeOptionsGUI();
        }

        private void OnDisable()
        {
            GameModeDropDown.onValueChanged.RemoveListener(OnGameModeSelected);
        }
        

        public void StartGame()
        {
            UIController.HandleStartGameModeClicked();
        }
        
        public void OnGameModeSelected(int index)
        {
            UIController.HandleSetActiveGameMode(index);
            AddScrollViewContent();
        }
    }
}