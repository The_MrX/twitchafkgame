﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.CoreProgram.GUI
{
    public class GUI_GameModeSettings : MonoBehaviour
    {
        //Should ideally in future add prefabs for toggles/text inputs etc and make the SetValue functions public
        //and then add them during start automatically instead of making them manually in the editor.
        //private RectTransform _content;

        private Dictionary<string, int> _intDict = new Dictionary<string, int>();
        private Dictionary<string, float> _floatDict = new Dictionary<string, float>();
        private Dictionary<string, string> _stringDict = new Dictionary<string, string>();
        private Dictionary<string, bool> _boolDict = new Dictionary<string, bool>();

        public int GetInt(string key)
        {
           return GetValue(_intDict, key);
        }
        
        public void SetInt(string key, int value)
        {
            SetValue(_intDict, key, value);
        }
        
        public string GetString(string key)
        {
            return GetValue(_stringDict, key);
        }
        
        public void SetString(string key, string value)
        {
            SetValue(_stringDict, key, value);
        }
        
        public bool GetBool(string key)
        {
            return GetValue(_boolDict, key);
        }
        
        public void SetBool(string key, bool value)
        {
            SetValue(_boolDict, key, value);
        }
        
        public float GetFloat(string key)
        {
            return GetValue(_floatDict, key);
        }
        
        public void SetFloat(string key, float value)
        {
            SetValue(_floatDict, key, value);
        }

        private void SetValue<T>(Dictionary<string, T> dict,  string key, T value)
        {
            if (!dict.ContainsKey(key))
            {
                dict.Add(key, value);
            }
            else
            {
                dict[key] = value;
            }
        }

        private T GetValue<T>(Dictionary<string, T> dict,  string key)
        {
            if (dict.ContainsKey(key))
            {
                return dict[key];
            }
            return default(T);
        }
        
        
    }
}

// public class ViewScrollContentController : MonoBehaviour
// {
//     private Dictionary<string, int> _intDict = new Dictionary<string, int>();
//     private Dictionary<string, float> _floatDict = new Dictionary<string, float>();
//     private Dictionary<string, string> _stringDict = new Dictionary<string, string>();
//     private Dictionary<string, bool> _boolDict = new Dictionary<string, bool>();
//     
//
//     public int GetInt(string key)
//     {
//        return GetValue(_intDict, key);
//     }
//     
//     private void SetInt(string key, int value)
//     {
//         SetValue(_intDict, key, value);
//     }
//     
//     public string GetString(string key)
//     {
//         return GetValue(_stringDict, key);
//     }
//     
//     private void SetString(string key, string value)
//     {
//         SetValue(_stringDict, key, value);
//     }
//     
//     public bool GetBool(string key)
//     {
//         return GetValue(_boolDict, key);
//     }
//     
//     private void SetBool(string key, bool value)
//     {
//         SetValue(_boolDict, key, value);
//     }
//     
//     public float GetFloat(string key)
//     {
//         return GetValue(_floatDict, key);
//     }
//     
//     private void SetFloat(string key, float value)
//     {
//         SetValue(_floatDict, key, value);
//     }
//
//     private void SetValue<T>(Dictionary<string, T> dict,  string key, T value)
//     {
//         if (!dict.ContainsKey(key))
//         {
//             dict.Add(key, value);
//         }
//         else
//         {
//             dict[key] = value;
//         }
//     }
//
//     private T GetValue<T>(Dictionary<string, T> dict,  string key)
//     {
//         if (dict.ContainsKey(key))
//         {
//             return dict[key];
//         }
//         return default(T);
//     }
// }


// public RectTransform Content
// {
//     get => _content;
//     set => _content = SetContent(value);
// }

// public void AddAllPropertiesInChildren()
// {
//     foreach (Transform child in transform)
//     {
//         var property = child.GetComponent<Property>();
//         if (property != null && !string.IsNullOrWhiteSpace(property.Key))
//         {
//             if (property.PropertyState == PropertyState.NotSet)
//             {
//                 return;
//             }
//             if (property.PropertyState == PropertyState.Bool)
//             {
//                 property.OnBoolValueChanged(SetBool(property.Key,property.BoolValue))
//                 SetBool(property.Key, property.BoolValue);
//             }
//             if (property.PropertyState == PropertyState.Float)
//             {
//                 SetFloat(property.Key, property.FloatValue);
//             }
//             if (property.PropertyState == PropertyState.String)
//             {
//                 SetString(property.Key, property.StringValue);
//             }
//             if (property.PropertyState == PropertyState.Int)
//             {
//                 SetInt(property.Key, property.IntValue);
//             }
//         }
//     }
// }

// private RectTransform SetContent(RectTransform rectTransform)
// {
//     //todo Verify This Somehow
//     return rectTransform;
// }