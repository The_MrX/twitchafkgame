﻿using System;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.CoreProgram.GUI
{
    public class GameModeSettingOption : MonoBehaviour
    {
        private static GUI_GameModeSettings _guiGameModeSettings;
        [Required]public string Identifier;

        [SerializeField]private PropertyState _propertyState = PropertyState.NotSet;
        public PropertyState PropertyState => _propertyState;


        private void Awake()
        {
            if (_guiGameModeSettings == null)
            {
                _guiGameModeSettings = GetComponentInParent<GUI_GameModeSettings>();
            }
            if (_propertyState == PropertyState.NotSet)
            {
                return;
            }
            if (_propertyState == PropertyState.Bool)
            {
                var toggle = GetComponent<Toggle>();
                if (toggle == null) { return; }
                toggle.onValueChanged.AddListener(OnValueChangedBool);
            }
            else
            {
                var textField = GetComponent<TMP_InputField>();
                if (textField == null) { return; }
                textField.onValueChanged.AddListener(ProcessValue);
            }
            
        }
        
        private void ProcessValue(string value)
        {
            if (_propertyState == PropertyState.String)
            {
                OnValueChangedString(value);
            }
            else if (_propertyState == PropertyState.Int)
            {
                if (Int32.TryParse(value, out var outIntValue))
                {
                    OnValueChangedInt(outIntValue);
                }
            }
            else if (_propertyState == PropertyState.Float)
            {
                if (float.TryParse(value, out var outFloatValue))
                {
                    OnValueChangedFloat(outFloatValue);
                }
            }
        }

        private void OnValueChangedBool(bool value)
        {
            _guiGameModeSettings.SetBool(Identifier, value);
        }
        
        private void OnValueChangedString(string value)
        {
            _guiGameModeSettings.SetString(Identifier, value);
        }
        
        private void OnValueChangedInt(int value)
        {
            _guiGameModeSettings.SetInt(Identifier, value);
        }
        
        private void OnValueChangedFloat(float value)
        {
            _guiGameModeSettings.SetFloat(Identifier, value);
        }
    }

    public enum PropertyState
    {
        NotSet,
        Bool,
        String,
        Int,
        Float
    }

}