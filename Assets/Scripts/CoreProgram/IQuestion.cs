﻿namespace Game.CoreProgram
{
    public interface IQuestion
    {
        string GetQuestion();
        string GetResult();
    }
}