﻿using System;
using UnityEngine;

namespace Game.CoreProgram
{
    //This Class handles both Input via mouse/keyboard etc. And all the main GUI elements are to call functions in this class.
    public class UserInput : MonoBehaviour
    {
        private void Update()
        {
            if (Time.timeScale == 0 ) { return; }
            
            //All UserInput Must be here
        }

        public void SetActiveGameMode()
        {
            //ProgramState.Singleton.SetActiveGameMode();
        }
        
        public void StartPlayingActiveGameMode()
        {
            ProgramState.Singleton.StartActiveGameMode();
        }
        
        public void PauseGameMode()
        {
            ProgramState.Singleton.PauseActiveGameMode();
        }
        
        public void UnPauseGameMode()
        {
            ProgramState.Singleton.ResumeActiveGameMode();
        }
    }
}