﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Audio;

namespace Game.CoreProgram
{
    public class SoundManager : MonoBehaviour
    {
        public static SoundManager Singleton;
        [SerializeField]private AudioMixer _allMixer;
        [SerializeField]private AudioMixer _musicMixer;
        [SerializeField]private AudioMixer _effectsMixer;

        [SerializeField] private float _fadeDuration = 0.25f;

        public float FadeDuration => _fadeDuration;

        private void Awake()
        {
            Singleton = this;
        }

        ///Volume from 0 to 1
        public void SetVolume(AudioMixerEnum mixer, float volume)
        {
            DOTweenModuleAudio.DOSetFloat(GetMixer(mixer), "Volume", GetVolumeInDb(volume), FadeDuration);
        }

        public float GetCurrentVolume(AudioMixerEnum mixer)
        {
             GetMixer(mixer).GetFloat("Volume", out var volume);
             return GetVolumePercent(volume);
        }

        ///Returns a volume of -80 to 20db from 0 to 1 float.
        private float GetVolumeInDb(float percent)
        {
            return (percent * 100) - 80;
        }
        
        private float GetVolumePercent(float dbVolume)
        {
            return (dbVolume + 80) * 0.01f;
        }

        private AudioMixer GetMixer(AudioMixerEnum mixer)
        {
            if (mixer == AudioMixerEnum.Master)
            {
                return _allMixer;
            }

            if (mixer == AudioMixerEnum.Music)
            {
                return _musicMixer;
            }

            if (mixer == AudioMixerEnum.Effects)
            {
                return _effectsMixer;
            }
            return null;
        }
    }
    
    public enum AudioMixerEnum
    {
        Master,
        Music,
        Effects
    }
}