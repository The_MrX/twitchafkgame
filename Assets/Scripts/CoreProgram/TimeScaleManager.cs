﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Game.CoreProgram
{
    public class TimeScaleManager : MonoBehaviour
    {
        public static TimeScaleManager _singleton;

        private Coroutine _coroutine;
        
        private void Awake()
        {
            _singleton = this;
        }

        public static void SetTimeScale(float speed)
        {
            Time.timeScale = speed;
        }
        
        public static void Pause()
        {
            Time.timeScale = 0;
        }
        
        public static void Unpause()
        {
            Time.timeScale = 1;
        }

        public void SetTimeScale_SetDesiredTimeScale(float desiredTimeScale, float duration, int numOfCycles = 10)
        {
            var changeAmount = desiredTimeScale - Time.timeScale;
            SetTimeScale_EaseInOut(changeAmount, duration, numOfCycles);
        }

        private void SetTimeScale_EaseInOut(float changeAmount, float duration, int numOfCycles = 10)
        {
            StopCoroutine(_coroutine);
            _coroutine = StartCoroutine(CR_EaseInOut(changeAmount,duration,numOfCycles ));
        }

        IEnumerator CR_EaseInOut(float changeAmount, float duration,  int numOfCycles)
        {
            if (duration == 0) { numOfCycles = 1; }
            float timePerCycle = duration / numOfCycles;
            float changePerCycle = changeAmount / numOfCycles;
            for (int i = 0; i < numOfCycles; i++)
            {
                Time.timeScale += changePerCycle;
                yield return new WaitForSeconds(timePerCycle);
            }
 
        }
    }
}