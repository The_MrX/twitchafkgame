﻿using DG.Tweening;
using UnityEngine;

namespace Game.CoreProgram
{
    public class AudioHandler : MonoBehaviour
    {
        private AudioSource _audioSource;
        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        public void PlayOneShot(AudioObject audioObject)
        {
            _audioSource.DOFade(audioObject.Volume, SoundManager.Singleton.FadeDuration);
            _audioSource.PlayOneShot(audioObject.AudioClip, audioObject.Volume);
            _audioSource.loop = false;
        }

        public void PlayOneShot(AudioClip clip, float volume)
        {
            _audioSource.DOFade(volume, SoundManager.Singleton.FadeDuration);
            _audioSource.PlayOneShot(clip, volume);
            _audioSource.loop = false;
        }
        
        public void SetVolume(float volume)
        {
            _audioSource.DOFade(volume, SoundManager.Singleton.FadeDuration);
        }
        
        public void PlayLooping(AudioObject audioObject)
        {
            _audioSource.DOFade(audioObject.Volume, SoundManager.Singleton.FadeDuration);
            _audioSource.clip = audioObject.AudioClip;
            _audioSource.loop = true;
        }
    }
}