﻿using System.Collections.Generic;
using Game.Saving;
using Game.GameModes;
using Game.TwitchIntegration;
using Game.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.CoreProgram
{
    public class ProgramState : MonoBehaviour
    {
        public static ProgramState Singleton;
        
        private TwitchApiInstance _twitchApi;
        private BotClient _botClient;
        [SerializeField]private List<BaseGameMode> _availableGameModes;
        private BaseGameMode _selectedGameMode;
        private BaseGameMode _activeGameMode;
        private SoundManager _soundManager;
        private UIManager _uiManager;
        private TwitchUsersData _twitchUsersData;

        public TwitchApiInstance TwitchApi => _twitchApi;
        public BotClient BotClient => _botClient;
        public BaseGameMode SelectedGameMode => _selectedGameMode;
        public SoundManager SoundManager => _soundManager;
        public UIManager UiManager => _uiManager;
        public TwitchUsersData TwitchUsersData => _twitchUsersData;
        public BaseGameMode ActiveGameMode => _activeGameMode;

        public List<BaseGameMode> AvailableGameModes => _availableGameModes;

        private void Awake()
        {
            Singleton = this;
            Application.runInBackground = true;
            DontDestroyOnLoad(this);
            
            _twitchApi = transform.GetComponentInChildren<TwitchApiInstance>();
            _botClient = transform.GetComponentInChildren<BotClient>();
            _soundManager = transform.GetComponentInChildren<SoundManager>();
            _uiManager = transform.GetComponentInChildren<UIManager>();

            _twitchUsersData = new TwitchUsersData();
            _twitchUsersData.Initialize();
        }

        private void Start()
        {
            //UtilDelayFunctionCall.Singleton.CallFunctionIn(StartGameMode, 1);
        }

        // private void StartGameMode()
        // {
        //     SetActiveGameMode("Word");
        //     StartActiveGameMode();
        // }

        public void SetActiveGameMode(int index)
        {
            _activeGameMode = _availableGameModes[index];
        }

        public void StartActiveGameMode()
        {
            _activeGameMode = Instantiate(_selectedGameMode);
            _activeGameMode.Initialize();
            _uiManager.HideMainMenu();
        }

        public void PauseActiveGameMode()
        {
            _activeGameMode.Pause();
        }
        
        public void ResumeActiveGameMode()
        {
            _activeGameMode.Resume();
        }
        
        public void AbortActiveGameMode()
        {
            _activeGameMode.EndSession();
        }
        
        public void CloseApplication()
        {
            Debug.Log("Close Application Called");
        }

        public string[] GetListOfGameModes()
        {
            List<string> names = new List<string>();
            foreach (var gameMode in _availableGameModes)
            {
                names.Add(gameMode.name);
            }
            return names.ToArray();
        }

        public RectTransform GetCurrentGameModeOptionsContent()
        {
            return _activeGameMode.OptionsContent;
        }
        //Sound Manager
        //ObjectPool Manager
        //UI Manager
    }
}