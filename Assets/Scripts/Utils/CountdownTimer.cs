﻿using UnityEngine;
namespace Game.Utils
{
    [System.Serializable]
    public class CountdownTimer
    {

        private float _whenIStartedCounting = Mathf.Infinity;
        private float _countdownDuration;
        
        private bool _countdownDurationIsDirty = false;
        private float _cacheCountdownDuration = 0f;
        
        public float CountdownTime()
        {
            if (_countdownDurationIsDirty)
            {
                return _cacheCountdownDuration;
            }
            return _countdownDuration;
        }

        public CountdownTimer(float duration)
        {
            _countdownDuration = duration;
        }

        public float GetTime()
        {
            return Time.time -_whenIStartedCounting;
        }
        
        public void ResetTimer()
        {
            this._whenIStartedCounting = Mathf.Infinity;
        }

        public bool HasStarted()
        {
            var time = GetTime();
            if (time > 0)
            {
                return true;
            }
            return false;
        }

        public bool HasFinished(bool resetTimerIfFinished = false)
        {
            var time = GetTime();
            if (time >= _countdownDuration)
            {
                if (resetTimerIfFinished) { ResetTimer(); }
                return true;
            }
            return false;
        }

        public void StartCountDown()
        {
            if (_countdownDurationIsDirty)
            {
                _countdownDuration = _cacheCountdownDuration;
                _cacheCountdownDuration = 0f;
                _countdownDurationIsDirty = false;
            }
            _whenIStartedCounting = Time.time;
        }
        
        /// <summary>
        /// UpdateInstantly to true will instantly change the duration of currently running countdowns.
        /// Instead of waiting for it to finish first.
        /// </summary>
        /// <param name="newDuration"></param>
        /// <param name="updateInstantly"></param>
        public void ChangeCountdownDuration(float newDuration, bool updateInstantly = false)
        {
            if (!updateInstantly && HasStarted())
            {
                _countdownDurationIsDirty = true;
                _cacheCountdownDuration = newDuration;
            }
            else
            {
                _countdownDuration = newDuration;
            }
        }
        
        public static void StartCountdown(CountdownTimer countdownTimer)
        {
            countdownTimer.ResetTimer();
            countdownTimer.StartCountDown();
        }
    }
}


    
