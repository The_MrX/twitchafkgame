﻿using System;
using System.Collections;
using UnityEngine;

namespace Game.Utils
{
    public class UtilDelayFunctionCall : MonoBehaviour
    {
        public static UtilDelayFunctionCall Singleton;
        private void Awake()
        {
            Singleton = this;
        }

        public void CallFunctionIn(Action methodToCall, float delay, bool repeating = false,  int maxNumOfLoops = 1)
        {
            StartCoroutine(CR_CallFunction(methodToCall,delay,repeating, maxNumOfLoops ));
        }

        IEnumerator CR_CallFunction(Action methodToCall, float delay,bool repeating, int maxNumOfLoops)
        {
            if (repeating)
            {
                for (int i = 0; i < maxNumOfLoops; i++)
                {
                    yield return new WaitForSeconds(delay);
                    methodToCall?.Invoke();
                }
            }
            else
            {
                yield return new WaitForSeconds(delay);
                methodToCall?.Invoke();
            }
        }
    }
}