﻿using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Game.FileProcessing
{
    public class SubwordsFileCreator
    {
        
        private List<string> wordsList;
        private Dictionary<int,List<SubwordContainer>> _dictionary = new Dictionary<int, List<SubwordContainer>>();
        private List<Thread> _threads = new List<Thread>();
        
        private bool isSaved = false;

        public void CreateSubWordsList()
        {
            Debug.Log("Started");
            wordsList= ES3.Load<List<string>>("words", @"D:\Unity Projects\Actual Projects\TwitchAfkGame\Assets\WordsFiles\USWordList.json");

            Debug.Log("Number of Words in List: "+wordsList.Count);
            int threadCount = 8;
            for (int i = 1; i <= threadCount; i++)
            {
                int index = i;
                Debug.Log("Creating Thread no. " + i);
                Thread thread = new Thread(() =>ParseWordsThread(index, threadCount));
                _threads.Add(thread);
                thread.Start();
            }
        }

        private void ParseWordsThread(int threadIndex, int numOfThreads)
        {
            var amountPerThread = Mathf.CeilToInt((wordsList.Count / numOfThreads));
            var endIndex = amountPerThread *  threadIndex;
            var startIndex = endIndex - amountPerThread;
            
            if (startIndex < 0) { startIndex = 0; }
            if (endIndex > wordsList.Count) { endIndex = wordsList.Count; }
            
            Debug.Log("Thread "+ threadIndex+ " startIndex: "+startIndex + " endIndex: "+ endIndex + " amountPerThread: "+ amountPerThread);
            var onePercent = Mathf.RoundToInt(amountPerThread/100f);
            List<SubwordContainer> subwordsToStore  = new List<SubwordContainer>();
            int index = 0;
            for (int i = startIndex; i < endIndex; i++)
            {
                if (index % onePercent == 0)
                {
                    Debug.Log("Thread "+ threadIndex + " Currently at "+ i + " from "+ endIndex + " percent: "+ Mathf.Round((index/(float)amountPerThread)*100f)/100f + "%");
                }

                var wordContainer = new SubwordContainer();
                wordContainer.Word = wordsList[i];
                foreach (var subWord in wordsList)
                {
                    if (DoesWordContainSubWord(subWord, wordsList[i]) && subWord != wordContainer.Word)
                    {
                        wordContainer.AddSubWord(subWord);
                    }
                }

                if (wordContainer.SubWords.Count > 0)
                {
                    subwordsToStore.Add(wordContainer);
                }
                index++;
            }
            _dictionary.Add(threadIndex, subwordsToStore);
            Debug.Log("Thread " + threadIndex + " is Finished");
        }

        private bool DoesWordContainSubWord(string subWord, string word)
        {
            int matchCount = 0;

            for (int i = 0; i < subWord.Length; i++)
            {
                for (int j = 0; j < word.Length; j++)
                {
                    if (subWord[i] == word[j])
                    {
                        matchCount++;
                        break;
                    }
                }
            }
            return matchCount == subWord.Length;
        }


        public void UpdateLoop()
        {
            SaveFileWhenThreadsFinish();
        }
        private void SaveFileWhenThreadsFinish()
        {
            Debug.Log("Number Of threads: "+_threads.Count);
            if (_threads.Count <= 0 || isSaved)
            {
                return;
            }

            foreach (var thread in _threads)
            {
                if (thread.IsAlive)
                {
                    return;
                }
            }

            Debug.Log("Threads Are Done, Saving");
            ES3Settings settings =
                new ES3Settings(@"D:\Unity Projects\Actual Projects\TwitchAfkGame\Assets\WordsFiles\SubWordsList.json");
            settings.prettyPrint = true;

            var subWords = new List<SubwordContainer>();
            foreach (var KVP in _dictionary)
            {
                foreach (var subword in KVP.Value)
                {
                    if (subword.SubWords.Count > 3)
                    {
                        subWords.Add(subword);
                    }
                }
            }
            subWords.Sort((container, subwordContainer) => container.SubWords.Count.CompareTo(subwordContainer.SubWords.Count) );

            ES3.Save("subwords", subWords, settings);
            Debug.Log(" Saving FINISHED!");
            isSaved = true;
        }
        
        public void DestroyThreads()
        {
            Debug.Log("Destroying Threads");
            foreach (var thread in _threads)
            {
                if (thread != null && thread.IsAlive)
                {
                    thread.Abort();
                }
            }
        }
    }
}