﻿using System;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Windows;
using File = System.IO.File;

namespace Game.FileProcessing
{
    public class FileProcessing : MonoBehaviour
    {
        [SerializeField] private TextAsset wordsFile;
        [SerializeField] private TextAsset namesFile;

        WordsListsPreProcessing preProcessing = new WordsListsPreProcessing();
        SubwordsFileCreator subwordsFileCreator = new SubwordsFileCreator();

        [SerializeField]private bool PreProcessWordsFIle = false;
        [SerializeField]private bool CreateSubwordsFile = false;
        
        private void Start()
        {
            // var wordsList = WordsListsPreProcessing.GetWordsFromTextFile(biggerNameList);
            // StringBuilder stringBuilder = new StringBuilder();
            // foreach (var word in wordsList)
            // {
            //     var adjustedWord = WordsListsPreProcessing.RemoveSymbols3(word);
            //     if (word.Length < 3)
            //     {
            //         continue;
            //     }
            //
            //     if (word.Contains("�") || word.Contains("/") || word.Contains("?"))
            //     {
            //         continue;
            //     }
            //     if (!string.IsNullOrWhiteSpace(adjustedWord))
            //     {
            //         stringBuilder.Append(word + "\n");
            //     }
            // }
            //
            // File.WriteAllText(@"D:\Unity Projects\Actual Projects\TwitchAfkGame\Assets\WordsFiles\EditedBiggerNameList.txt", 
            //     stringBuilder.ToString(), Encoding.UTF8);
            
            if (PreProcessWordsFIle)
            {
                WordsPreprocessing();
            }

            if (CreateSubwordsFile)
            {
                subwordsFileCreator.CreateSubWordsList();
            }
        }

        private void Update()
        {
            if (CreateSubwordsFile)
            {
                subwordsFileCreator.UpdateLoop();
            }
        }

        private void WordsPreprocessing()
        {
            var words = WordsListsPreProcessing.GetWordsFromTextFile(wordsFile);
            
            var list = new List<string[]>();
            list.Add(WordsListsPreProcessing.GetWordsFromTextFile(namesFile));

            StartCoroutine(preProcessing.FilterWordList("USWordList", words, list));
        }



        private void OnDestroy()
        {
            if (CreateSubwordsFile)
            {
                subwordsFileCreator.DestroyThreads();
            }
        }
    }

    [Serializable]
    public class SubwordContainer
    {
        public string Word;
        public List<string> SubWords = new List<string>();
        

        public void AddSubWord(string word)
        {
            SubWords.Add(word);
        }
    }
    
}