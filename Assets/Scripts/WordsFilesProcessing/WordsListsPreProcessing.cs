﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Game.FileProcessing
{
    public class WordsListsPreProcessing
    {
        
        public static string[] GetWordsFromTextFile(TextAsset textAsset)
        {
            return textAsset.text.Split(new[] {"\r\n", "\r", "\n", "$", "#", ",", " ", "|", "+"}, StringSplitOptions.RemoveEmptyEntries);
        }
        
        public IEnumerator FilterWordList(string fileName, string[] wordsList, List<string[]> filterLists )
        {
            Debug.Log("Started File Processing");
            var words = wordsList;
            List<string> filterWords =  new List<string>();
            foreach (var filterList in filterLists)
            {
                filterWords.AddRange(filterList);
            }
            Debug.Log("Words: " + words.Length);
            List<string> wordsToSave = new List<string>();
            bool addWord = true;
            var progress = 0;
            foreach (var word in words)
            {
                progress++;
                addWord = true;
                var cleanWord = RemoveSymbols(word);
                
                if (cleanWord.Length < 4)
                {
                    addWord = false;
                }

                foreach (var filterWord in filterWords)
                {
                    if (addWord == false) { break; }
                    if (cleanWord == filterWord)
                    {
                        addWord = false;
                    }
                }

                if (addWord)    
                {
                    wordsToSave.Add(cleanWord.ToLower());
                }
                
                if (progress % 10000 == 0)
                {
                    yield return null;
                }
                
            }
            
            Debug.Log("Number of Words: " + wordsToSave.Count);
            for (int i = 0; i < 50; i++)
            {
                Debug.Log("Word: "+wordsToSave[i]);
            }

            ES3Settings settings = new ES3Settings(@"D:\Unity Projects\Actual Projects\TwitchAfkGame\Assets\WordsFiles\"+ fileName + ".json");
            settings.prettyPrint = true;
            ES3.Save("words", wordsToSave, settings);
            Debug.Log("Finished File Processing");
            
        }
        
        public static string RemoveSymbols(string word)
        {
            var charArray = word.ToCharArray();

            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < charArray.Length; i++)
            {
                if (charArray[i] == '^'
                    || charArray[i] == '-'
                    || charArray[i] == '?'
                    || charArray[i] == '?'
                    || charArray[i] == '-'
                    || charArray[i] == '!'
                    || charArray[i] == '#'
                    || charArray[i] == '\\'
                    || charArray[i] == '&'
                    || charArray[i] == '+'
                    || charArray[i] == '('
                    || charArray[i] == ')'
                    || charArray[i] == ']'
                    || charArray[i] == '['
                    || charArray[i] == '}'
                    || charArray[i] == '{'
                    || charArray[i] == '.'
                    || charArray[i] == '<'
                    || charArray[i] == '/'
                    || charArray[i] == '>'
                    || charArray[i] == ','
                    || charArray[i] == '1'
                    || charArray[i] == '2'
                    || charArray[i] == '3'
                    || charArray[i] == '4'
                    || charArray[i] == '5'
                    || charArray[i] == '6'
                    || charArray[i] == '7'
                    || charArray[i] == '8'
                    || charArray[i] == '9'
                    || charArray[i] == '0')
                {
                    
                }
                else
                {
                    if (charArray[i] == '/')
                    {
                        break;
                    }
                    else
                    {
                        stringBuilder.Append(charArray[i]);
                    }
                }
            }
            return stringBuilder.ToString();
        }
        
        public static string RemoveSymbols2(string word)
        {
            var charArray = word.ToCharArray();
            
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < charArray.Length; i++)
            {
                if (charArray[i] >= 'a' && charArray[i] <= 'z')
                {
                    stringBuilder.Append(charArray[i]);
                }
            }
            return stringBuilder.ToString();
        }
        
        public static string RemoveSymbols3(string word)
        {
            var charArray = word.ToCharArray();
            
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < charArray.Length; i++)
            {
                if (Char.IsLetter(charArray[i]))
                {
                    stringBuilder.Append(charArray[i]);
                }
            }
            return stringBuilder.ToString();
        }
    }
    
    
}