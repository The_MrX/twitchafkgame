﻿using System.Collections.Generic;
using System.Text;
using Game.CoreProgram;
using UnityEngine;

namespace Game.GameModes
{
    public class WordContainer : UIObject
    {
        private List<LetterContainer> _letterContainers = new List<LetterContainer>();

        private GuessWord _guessWord;

        public GuessWord GuessWord => _guessWord;

        private int _numberOfWords;

        private void Start()
        {
            foreach (Transform trans in transform)
            {
                _letterContainers.Add(trans.GetComponent<LetterContainer>());
            }
        }

        public void SetWordData(GuessWord guessWord)
        {
            if (_letterContainers.Count == 0)
            {
                Start();
            }
            Debug.Log("Word is: "+guessWord.word);
            _guessWord = guessWord;
            SetLettersEmpty();
            _guessWord.OnWordFound += OnWordFound;
        }
        
        public void SetWord_Override(string text)
        {
            if (_letterContainers.Count == 0)
            {
                Start();
            }
            SetLettersText(text);
        }

        private void SetLettersEmpty()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < _guessWord.word.Length; i++)
            {
                stringBuilder.Append(" ");
            }
            SetLettersText(stringBuilder.ToString());
        }

        private void OnWordFound()
        {
            SetLettersText(_guessWord.word);
        }

        private void SetLettersText(string text)
        {
            var endIndex = _letterContainers.Count - text.Length-1;
            var charIndex = text.Length-1;
            for (int i = _letterContainers.Count-1 ; i > endIndex; i--)
            {
                _letterContainers[i].ShowLetter();
                _letterContainers[i].SetText(text[charIndex]);
                charIndex--;
            }
        }
    }
}