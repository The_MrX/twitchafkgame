﻿using System.Collections.Generic;
using Game.CoreProgram;
using UnityEngine;

namespace Game.GameModes
{
    public class WordsColumn : UIObject
    {
        List<WordContainer> _wordContainers = new List<WordContainer>();
        private int _maxNumOfLetters = 0;

        public int MaxNumOfLetters => _maxNumOfLetters;

        private void Awake()
        {
            foreach (Transform trans in transform)
            {
                _wordContainers.Add(trans.GetComponent<WordContainer>());
            }
            
            foreach (Transform trans in transform.GetChild(0).transform)
            {
                _maxNumOfLetters = MaxNumOfLetters + 1;
            }
        }

        public void AddWords(List<GuessWord> guessWords)
        {
            var difference = _wordContainers.Count - guessWords.Count;
            if (difference <= -1)
            {
                Debug.Log("Error: more words than space to put them");
            }
            var startIndex = 0;
            if (difference > 0)
            {
                startIndex = Random.Range(0, difference); // start at random slot, if possible, to make it looks a bit more interesting.
            }
            for (int i = 0; i < guessWords.Count; i++)
            {
                _wordContainers[startIndex+i].SetWordData(guessWords[i]);
            }
        }
    }
}