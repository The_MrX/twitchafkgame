﻿using System;
using System.Text;
using Game.Utils;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.GameModes
{
    public class PreviewWordContainer : MonoBehaviour
    {
        private PreviewWord _previewWord;
        private WordContainer _wordContainer;
        public float _delayPerShuffle;
        public int _numOfIterations;

        private void Awake()
        {
            _wordContainer = transform.GetChild(0).GetComponent<WordContainer>();
        }

        private void SetPreviewWord(string word)
        {
            _previewWord = new PreviewWord(word);
        }

        public void StartWordPreview(string word, float gameDuration, float previewUpdatedelay)
        {
            _delayPerShuffle = previewUpdatedelay;
            _numOfIterations = Mathf.FloorToInt(gameDuration / previewUpdatedelay);

            SetPreviewWord(word);
            UpdatePreviewGUI();
            UtilDelayFunctionCall.Singleton.CallFunctionIn(UpdatePreviewGUI, _delayPerShuffle, true, _numOfIterations);
        }

        private void UpdatePreviewGUI()
        {
            if (_previewWord == null) { return; }
            _wordContainer.SetWord_Override(_previewWord.GetShuffledLetterWord());
        }
    }
    
    public class PreviewWord
    {
        public PreviewWord(string word)
        {
            _word = word;
        }
        private string _word;

        public string Word => _word;

        public string GetShuffledLetterWord()
        {
            StringBuilder stringBuilder = new StringBuilder(Word);
            var length= stringBuilder.Length;
            for (int i = 0; i < length; i++)
            {
                var ranNum = Random.Range(0, length);
                var tempChar = stringBuilder[i];
                stringBuilder[i] = stringBuilder[ranNum];
                stringBuilder[ranNum] = tempChar;
            }
            return stringBuilder.ToString();
        }
    }
}