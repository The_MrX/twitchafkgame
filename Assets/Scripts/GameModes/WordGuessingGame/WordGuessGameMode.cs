﻿using System;
using System.Collections.Generic;
using System.Text;
using Game.CoreProgram;
using TwitchLib.Client.Models;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.GameModes
{
    public class WordGuessGameMode : BaseGameMode
    {
        [SerializeField] private int _points;
        [SerializeField] private float _gracePeriod = 5;
        [SerializeField] private WordsColumn[] wordsColumnContainers;
        [SerializeField] private PreviewWordContainer _previewWordContainer;
        
        private List<GuessWord> _guessWords = new List<GuessWord>();


        protected override void ProcessChatMessage(ChatMessage chatMessage)
        {
            if (!_roundStarted || !_isInitialized)
            {
                return;
            }

            foreach (var guessWord in _guessWords)
            {
                if (guessWord.Found == true)
                {
                    continue;
                }

                var word = guessWord.word.ToLower();
                if (chatMessage.Message.ToLower() == word)
                {
                    guessWord.SetFound();
                    WordFound(chatMessage.UserId, word);
                    return;
                }
            }
        }

        private void Update()
        {

        }

        void WordFound(string userId, string word)
        {
            Debug.Log(
                "Word Found: " + word + " from user: " + ProgramState.Singleton.TwitchUsersData.GetTwitchUserName(userId));
            GivePoints(userId, _points);
            CheckIfAllWordsFound();
        }

        private void CheckIfAllWordsFound()
        {
            foreach (var guessWord in _guessWords)
            {
                if (guessWord.Found == false)
                {
                    return;
                }
            }

            Debug.Log("All Words Found");
            ShowEndScreen();
        }

        public override void Initialize()
        {
            base.Initialize();

            SetHintWord("AppleCatDog");
            
            AddWord("Cat");
            AddWord("Dog");
            AddWord("Apple");
            AddWord("PineApple");
            AddWord("Pie");
            AddWord("MickyMouse");

            SortGuessWords();

            int wordIndex = 0;
            for (int i = 0; i < wordsColumnContainers.Length; i++)
            {
                var guessWordsToSend = new List<GuessWord>();
                int remainingWords = _guessWords.Count - wordIndex;
                var average = Mathf.FloorToInt(remainingWords /(float)(wordsColumnContainers.Length - i));
                for (int j = 0; j < average; j++)
                {
                    if (_guessWords[wordIndex].word.Length > wordsColumnContainers[i].MaxNumOfLetters)
                    {
                        break;
                    }
                    guessWordsToSend.Add(_guessWords[wordIndex]);
                    wordIndex++;
                }
                wordsColumnContainers[i].AddWords(guessWordsToSend);
            }
        }

        private void SortGuessWords()
        {
            _guessWords.Sort((guessWordOne, guessWordTwo) =>
                guessWordOne.word.Length.CompareTo(guessWordTwo.word.Length));
        }


        private void SetHintWord(string text)
        {
            _previewWordContainer.StartWordPreview(text, _timerDuration, 10f );
        }
        
        private void AddWord(string word)
        {
            var guessWord = new GuessWord(word);
            _guessWords.Add(guessWord);
        }

        protected override void StartRound()
        {
            base.StartRound();
        }

        public override void Pause()
        {
            base.Pause();
        }

        public override void Resume()
        {
            base.Resume();
        }
        
        
        public override void RestartRound()
        {
            base.RestartRound();
        }
        
        protected override void ShowEndScreen()
        {
            //todo check all points were added.
            base.ShowEndScreen();
        }
        
        protected override void GivePoints(string twitchUserId, int pointsToAdd)
        {
            base.GivePoints(twitchUserId, pointsToAdd);
        }

        public override void EndSession()
        {
            base.EndSession();
        }    
    }

    [Serializable]
    public class GuessWord
    {
        public string word;
        private bool found = false;

        public Action OnWordFound;

        public GuessWord(string word)
        {
            this.word = word;
        }

        public bool Found => found;

        public void SetFound()
        {
            found = true;
            OnWordFound?.Invoke();
        }
    }
}