﻿using Game.CoreProgram;
using TMPro;

namespace Game.GameModes
{
    public class LetterContainer : UIObject
    {
        private TextMeshProUGUI _textTMP;
        private UnityEngine.UI.Image _image;

        private void Awake()
        {
            _textTMP = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            _image = transform.GetComponent<UnityEngine.UI.Image>();
            HideLetter();
        }
        
        public void SetText(char text)
        {
            _textTMP.SetText(text.ToString().ToLower());
        }

        public void HideLetter()
        {
            _image.enabled =false;
            _textTMP.enabled = false;
        }
        
        public void ShowLetter()
        {
            _image.enabled = true;
            _textTMP.enabled = true;
        }
    }
}