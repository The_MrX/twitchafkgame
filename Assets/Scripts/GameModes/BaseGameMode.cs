﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.CoreProgram;
using Game.TwitchIntegration;
using Game.Utils;
using Sirenix.OdinInspector;
using TwitchLib.Api;
using TwitchLib.Client.Models;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.GameModes
{
    //Base Game Mode Class, that all GameModes will inherit from, this will contain the core functionality of every game mode.
    public abstract class BaseGameMode : MonoBehaviour
    {
        //Contacts Sound Manager indirectly
        //GameModeUI Manager
        [SerializeField]protected string _name;
        [SerializeField]protected string _description;
        [SerializeField]protected Texture _imagePreview;
        [SerializeField]protected float _startDelay = 5f;
        [SerializeField]protected float _timerDuration = 60f;
        [SerializeField]protected float _syncDelay = 5f;
        [SerializeField]protected RectTransform _optionsContent;
        
        protected bool _isInitialized;
        protected bool _roundStarted;
        protected bool _isPaused;
        private Dictionary<string, int> _pointsThisRound;
    
        protected CountdownTimer _gameModeCountdownTimer;

        public string Name => _name;
        public string Description => _description;
        public Texture ImagePreview => _imagePreview;
        public RectTransform OptionsContent => _optionsContent;

        public virtual void Initialize()
        {
            _isInitialized = true;
            BotClient.Singleton.OnChatMessageRecieved += ProcessChatMessage;
            UtilDelayFunctionCall.Singleton.CallFunctionIn(StartRound, _startDelay);
            _gameModeCountdownTimer = new CountdownTimer(_timerDuration);
        }
        
        protected virtual void StartRound()
        {
            if (!_isInitialized || _roundStarted)
            {
                return;
            }
            _roundStarted = true;
            _pointsThisRound = new Dictionary<string, int>();
            StartRoundTimer();
            Debug.Log("Round Started!");
        }

        public virtual void Pause()
        {
            _isPaused = true;
            TimeScaleManager.Pause();
        }

        public virtual void Resume()
        {
            _isPaused = false;
            TimeScaleManager.Unpause();
        }

        protected virtual void ProcessChatMessage(ChatMessage message)
        {
            
        }

        protected virtual void ShowEndScreen()
        {
            if (!_roundStarted || !_isInitialized)
            {
                return;
            }

            UtilDelayFunctionCall.Singleton.CallFunctionIn(EndRound, _syncDelay);
            Debug.Log("Show End Screen");
        }

        protected virtual void GivePoints(string twitchUserId, int pointsToAdd)
        {
            ProgramState.Singleton.TwitchUsersData.AddPoints(twitchUserId,pointsToAdd);
            if (!_pointsThisRound.ContainsKey(twitchUserId))
            {
                _pointsThisRound.Add(twitchUserId, 0);
            }
            _pointsThisRound[twitchUserId] += pointsToAdd;
        }

        protected void StartRoundTimer()
        {
            //Need to make sure these are in sync always.
            UtilDelayFunctionCall.Singleton.CallFunctionIn(ShowEndScreen, _timerDuration);
            _gameModeCountdownTimer.StartCountDown();
        }

        public virtual void EndRound()
        {
            if (!_roundStarted)
            {
                return;
            }
            _roundStarted = false;
            _gameModeCountdownTimer.ResetTimer();
            Debug.Log("End Round");
        }
        
        public virtual void RestartRound()
        {
            if (!_isInitialized || _roundStarted)
            {
                return;
            }
        }
        
        public virtual void EndSession()
        {
            
        }
        
        public virtual float GetCurrentCountDownTime()
        {
            if (!_roundStarted)
            {
                return -1;
            }
            return _gameModeCountdownTimer.GetTime();
        }
    }
    
}