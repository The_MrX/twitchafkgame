﻿using System;
using System.Collections.Generic;
using TwitchLib.Client.Events;
using TwitchLib.Client.Models;
using TwitchLib.Unity;
using UnityEngine;

namespace Game.TwitchIntegration
{
    public class BotClient : MonoBehaviour
    {
        public static BotClient Singleton;
        [SerializeField] private string _botTwitchName;
        [SerializeField] private string _twitchChannelName;

        private Client _client;
        public Client Client => _client;
        public string BotTwitchName => _botTwitchName;
        public string TwitchChannelName => _twitchChannelName;
        public Action<ChatMessage> OnChatMessageRecieved;
        
        private Queue<ChatMessage> _recentMessages = new Queue<ChatMessage>();
        private void Awake()
        {
            Singleton = this;
        }

        private void Start()
        {
            if (string.IsNullOrWhiteSpace(_twitchChannelName))
            {
                Debug.Log("ERROR: ChannelName cannot be Null");
                return;
            }
            _client = new Client();
            var connectionCred = new ConnectionCredentials(BotTwitchName,AuthenticationData.Singleton.TokensAccessToken );
            _client.Initialize(connectionCred, TwitchChannelName.ToLower());
            
            //todo Subscribe Events here.
            _client.OnMessageReceived += HandleMessageRecieved;
            
            _client.Connect();
        }
        
        private void HandleMessageRecieved( object sender, OnMessageReceivedArgs args)
        {
            Debug.Log("Message Received from: " + args.ChatMessage.Username +" :\n" + args.ChatMessage.Message);
            LogMessage(args.ChatMessage);
            OnChatMessageRecieved?.Invoke(args.ChatMessage);
        }

        private void LogMessage(ChatMessage chatMessage)
        {
            if (_recentMessages.Count >= 100)
            {
                _recentMessages.Dequeue();
            }
            _recentMessages.Enqueue(chatMessage);
        }
        
        public bool GetRecentChatMessage(string userId, out ChatMessage outChatMessage)
        {
            foreach (var chatMessage in _recentMessages)
            {
                if (chatMessage.UserId == userId)
                {
                    outChatMessage = chatMessage;
                    return true;
                }
            }
            outChatMessage = null;
            return false;
        }
    }
}