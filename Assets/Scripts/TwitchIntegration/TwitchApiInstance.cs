﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TwitchLib.Api.Core.Models.Undocumented.Chatters;
using TwitchLib.Unity;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.TwitchIntegration
{
    public class TwitchApiInstance : MonoBehaviour
    {
        public static TwitchApiInstance _instance;
        private Api _twitchApi;
        public Api TwitchApi => _twitchApi;

        [FormerlySerializedAs("_requestSent")] public bool _requestPending = false;

        private void Awake()
        {
            _instance = this;
        }

        void Start()
        {
            _twitchApi = new Api();
            _twitchApi.Settings.ClientId = AuthenticationData.Singleton.ApplicationClientId;
            _twitchApi.Settings.Secret = AuthenticationData.Singleton.ApplicationClientSecret;
            _twitchApi.Settings.AccessToken = AuthenticationData.Singleton.TokensAccessToken;

            //_twitchApi.Settings.PropertyChanged += HandlePropertyChanged;

        }


        private void GetChattersListCallback(List<ChatterFormatted> response)
        {
            foreach (var user in response)
            {
                Debug.Log("User: " + user.Username);
            }

            _requestPending = false;
        }

        void Update()
        {
            // if (!_requestPending && Input.GetKeyDown(KeyCode.Alpha1))
            // {
            //     _requestPending = true;
            //     StartCoroutine(GetUsersList("savag3fam"));
            // }
        }

        IEnumerator GetUsersList(string channelName)
        {
            Debug.Log("Before");
            yield return _twitchApi.InvokeAsync(_twitchApi.Undocumented.GetChattersAsync(channelName),
                GetChattersListCallback);
            Debug.Log("After");
        }

        void HandlePropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            Debug.Log("Property Changed Called");
        }


        private void OnDestroy()
        {

        }
    }
}
