﻿using UnityEngine;

namespace Game.TwitchIntegration
{
    public class AuthenticationData : MonoBehaviour
    {
        public static AuthenticationData Singleton;

        // https://dev.twitch.tv/docs/authentication
        [SerializeField] private string Application_ClientSecret;
        [SerializeField] private string Application_ClientId;
        [SerializeField] private string Tokens_AccessToken;
        [SerializeField] private string Tokens_RefreshToken;
        [SerializeField] private string Tokens_ClientId;

        public string ApplicationClientSecret => Application_ClientSecret;
        public string ApplicationClientId => Application_ClientId;
        public string TokensAccessToken => Tokens_AccessToken;
        public string TokensRefreshToken => Tokens_RefreshToken;
        public string TokensClientId => Tokens_ClientId;

        private void Awake()
        {
            Singleton = this;
        }
    }
}
