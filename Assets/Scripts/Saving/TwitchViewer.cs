﻿using System;
using TwitchLib.Client.Models;
using UnityEngine;

namespace Game.Saving
{
    [Serializable]
    public class TwitchViewer : TwitchUser
    {
        private int points;
        public TwitchUserFlags TwitchUserFlags;
        private DateTime _lastInputRecieved;
        
        public int Points => points;
        public DateTime LastInputRecieved => _lastInputRecieved;

        
        private int _tempInt;
        private TimeSpan _tempTS;

        public void SetFlags(TwitchUserFlags flags)
        {
            TwitchUserFlags = (TwitchUserFlags | flags) & flags;
        }

        public bool ChangePoints(int amount)
        {
            _tempInt = points + amount;
            if (_tempInt < 0)
            {
                return false;
            }
            points += _tempInt;
            UpdateLastInputReceived();
            return true;
        }
        
        public void UpdateLastInputReceived()
        {
            _lastInputRecieved = DateTime.Now;
        }

        public int DaysSinceUpdate()
        {
            _tempTS = _lastInputRecieved - DateTime.Now;
            return Mathf.RoundToInt((float)_tempTS.TotalDays);
        }

        public void SetData(ChatMessage chatMessage)
        {
            Id = chatMessage.UserId;
            UserName = chatMessage.Username;
            _lastInputRecieved = DateTime.Now;
            if (chatMessage.Bits > 0)
            {
                SetFlags(TwitchUserFlags.BitGiver);
            }
            if (chatMessage.IsSubscriber)
            {
                SetFlags(TwitchUserFlags.Sub);
            }
            if (chatMessage.IsModerator)
            {
                SetFlags(TwitchUserFlags.Mod);
            }
            if (chatMessage.IsVip)
            {
                SetFlags(TwitchUserFlags.VIP);
            }
        }
    }
    
    [Flags]
    public enum TwitchUserFlags
    {
        Follower = 1,
        Sub = 2,
        VIP = 4,
        Mod = 8,
        Gifter = 16,
        BitGiver = 32
    }
}