﻿using System;
using System.Collections.Generic;
using Game.TwitchIntegration;
using TwitchLib.Client.Models;
using TwitchLib.Unity;
using UnityEngine;

namespace Game.Saving
{
    [Serializable]
    public class TwitchUsersData
    {
        private Dictionary<string, TwitchViewer> _twitchViewers = new Dictionary<string, TwitchViewer>();

        private string _fileName = @"TwitchUsersData.json";
        private string _key = "UsersData";
        
        //todo At some point need to make it so it doesn't load every user, incase that's 1000s of users...
        public void Initialize()
        {
            BotClient.Singleton.OnChatMessageRecieved += HandleMessageReceived;
            LoadData();
        }

        public void SaveData()
        {
            //Sort by points and
            ES3.Save(_key, _twitchViewers, _fileName);
        }

        private bool HasTwitchUser(string userId)
        {
            return _twitchViewers.ContainsKey(userId);
        }
        
        public void LoadData()
        {
            if (ES3.FileExists(_fileName))
            {
                _twitchViewers = ES3.Load<Dictionary<string, TwitchViewer>>(_key, _fileName);
            }
        }
        
        public int GetCurrentPoints(string userId)
        {
            return _twitchViewers[userId].Points;
        }
        
        public string GetTwitchUserName(string userId)
        {
            return _twitchViewers[userId].UserName;
        }
        
        public void AddPoints(string userId, int points)
        {
            if (!HasTwitchUser(userId))
            {
                Debug.Log("Error: no user with the userId exists");
                return;
            }
            if (_twitchViewers[userId].DaysSinceUpdate() > 1)
            {
                BotClient.Singleton.GetRecentChatMessage(userId, out var outChatMessage);
                _twitchViewers[userId].SetData(outChatMessage);
            }
            _twitchViewers[userId].ChangePoints(points);
        }

        private void CreateUser(ChatMessage chatMessage)
        {
            var twitchViewer = new TwitchViewer();
            twitchViewer.SetData(chatMessage);
            _twitchViewers.Add(twitchViewer.Id, twitchViewer);
        }

        private void HandleMessageReceived(ChatMessage chatMessage)
        {
            if (!HasTwitchUser(chatMessage.UserId))
            {
                CreateUser(chatMessage);
            }
        }
    }
}