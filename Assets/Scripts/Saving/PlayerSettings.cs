﻿using UnityEngine;
using Resolution = Game.CoreProgram.Resolution;

namespace Game.Saving
{
    public class PlayerSettings
    {
        private FullScreenMode _fullScreenMode;
        private Resolution _resolution;
        private float _masterVolume;
        private float _effectsVolume;
        private float _musicVolume;

        public FullScreenMode FullScreenMode => _fullScreenMode;
        public Resolution Resolution1 => _resolution;
        public float MasterVolume => _masterVolume;
        public float EffectsVolume => _effectsVolume;
        public float MusicVolume => _musicVolume;

        public const string ScreenModeKey = "FullScreenMode";
        public const string ResolutionKey = "Resolution";
        public const string MasterVolumeKey = "MasterVolume";
        public const string EffectsVolumeKey = "EffectsVolume";
        public const string MusicVolumeKey = "MusicVolume";

        public void LoadData()
        {
            _fullScreenMode = ES3.Load<FullScreenMode>(ScreenModeKey);
            _resolution = ES3.Load<Resolution>(ResolutionKey);
            _masterVolume = ES3.Load<float>(MasterVolumeKey);
            _effectsVolume = ES3.Load<float>(EffectsVolumeKey);
            _musicVolume = ES3.Load<float>(MusicVolumeKey);
        }
        
        public void SaveData()
        {
            ES3.Save(ScreenModeKey, _fullScreenMode);
            ES3.Save(ResolutionKey, _resolution);
            ES3.Save(MasterVolumeKey, _masterVolume);
            ES3.Save(EffectsVolumeKey, _effectsVolume);
            ES3.Save(MusicVolumeKey, _musicVolume);
        }
    }
}