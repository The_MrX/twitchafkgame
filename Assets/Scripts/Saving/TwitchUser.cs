﻿using System;

namespace Game.Saving
{
    [Serializable]
    public class TwitchUser
    {
        public string UserName;
        public string Id;
    }
}